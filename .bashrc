export PATH="$PATH:~/scripts"
red='\[\e[0;31m\]'
RED='\[\e[1;31m\]'
blue='\[\e[0;34m\]'
BLUE='\[\e[1;34m\]'
cyan='\[\e[0;36m\]'
CYAN='\[\e[1;36m\]'
green='\[\e[0;32m\]'
GREEN='\[\e[1;32m\]'
yellow='\[\e[0;33m\]'
YELLOW='\[\e[1;33m\]'
purple='\[\e[0;35m\]'
PURPLE='\[\e[1;35m\]'
NOCOL='\[\e[0m\]'


### variables ###
OS=$(uname)             # for resolving pesky os differing switches

export EDITOR="vi"
export VISUAL="vi"
export HISTCONTROL=erasedups
export HISTSIZE=100000

# Enable options:
shopt -s cdspell         # auto fixes cd / spelling mistakes
shopt -s cdable_vars
shopt -s checkhash
shopt -s checkwinsize
shopt -s cmdhist
shopt -u mailwarn
shopt -s sourcepath
shopt -s no_empty_cmd_completion  # bash>=2.04 only
shopt -s histappend histreedit histverify
shopt -s extglob    # necessary for programmable completion

### aliases ###
alias ls="ls --color"
alias l="ls -lh --color"
alias ll="ls -lh --color"
alias la="ls -lha --color"
alias msg='sudo tail -n100 /var/log/messages'
alias msgf='sudo tail -f -n50 /var/log/messages'
alias sys='sudo tail -n100 /var/log/syslog'
alias sysf='sudo tail -f -n50 /var/log/syslog'
alias lg='cd /var/log'
alias ini='cd /etc/init.d'
alias s='sudo '
alias si='sudo -i'
alias gre='grep -Ev "^$|^#"'
alias proxyoff='export http_proxy=""; export https_proxy=""'
alias dfh='df -h -x squashfs'
alias lsl='ls -ltrh'


alias sshagent='eval $(ssh-agent); ssh-add ~/.ssh/id_rsa'
#if [ -z "$SSH_AUTH_SOCK" ] ; then
#  eval `ssh-agent -s`
#  ssh-add
#fi

### options ###
#set -o notify       # notify when jobs running in background terminate
#set -o noclobber    # prevents catting over file
set -o vi


#autocomplete ssh commands
#This gets the autocomplete from the known_hosts.
#complete -W "$(echo `cat ~/.ssh/known_hosts | cut -f 1 -d ' ' | sed -e s/,.*//g | uniq | grep -v "\["`;)" ssh

PATH="$PATH:~/bin"
if [ "$UID" -eq 0 ]; then
	USER_COLOUR="$RED"
else
	USER_COLOUR="$NOCOL"
fi
H=$(hostname -s)
case $H in
	b*) 
		HOST_COLOUR="$YELLOW"
	;;
	*)
		HOST_COLOUR="$CYAN"
	;;
esac

	



parse_git_branch() {
    git branch 2> /dev/null | awk '/\*/{print "(" $2 ")"}'
}
GIT_COLOUR="$CYAN"

#DEFAULT_PS1="${USER_COLOUR}\u${NOCOL}@${HOST_COLOUR}\h ${NOCOL}[ \w ] ${GIT_COLOUR}\$(parse_git_branch)${USER_COLOUR} \\$ ${NOCOL}"
# http://shapecatcher.com/unicode/block/Miscellaneous_Symbols

DEFAULT_PS1=$"${USER_COLOUR}\u${NOCOL}@${HOST_COLOUR}\h ${NOCOL}[ \w ] ${GIT_COLOUR}\$(parse_git_branch)${USER_COLOUR} $(printf '\xe2\x9a\xa1') \\$ ${NOCOL}"

# powerline-shell
# https://github.com/b-ryan/powerline-shell
function _update_ps1() {
    which powerline-shell >/dev/null 2>&1
    PWL=$?
    if [[ $PWL -eq 0 ]]; then
        PS1=$(powerline-shell $? 2>/dev/null 1>&1)
    else
	PS1=$DEFAULT_PS1
    fi
}

#if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then
if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then
    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
else
    PS1=$DEFAULT_PS1
    #PS1="${USER_COLOUR}\u${NOCOL}@${HOST_COLOUR}\h ${GIT_COLOUR}( \$(parse_git_branch) ) ${NOCOL}[ \w ] ${USER_COLOUR}\\$ ${NOCOL}"
fi
if [ -f $HOME/.dir_colors ]; then
    eval `dircolors $HOME/.dir_colors`
fi

setWindowTitle() {
	echo -ne "\e]2;$*\a"
	}

[[ ! -f ~/.aliases ]] || source ~/.aliases

# homebrew
if [[ $UID -ne 0 ]]; then
	test -f /home/linuxbrew/.linuxbrew/bin/brew && eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
fi
# now fix up the ansible binary that complains when brew PATH precedes the OS one:
export PATH="/usr/bin:$PATH"
