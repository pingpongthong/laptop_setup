# SHELL := /usr/bin/bash
aliases:
	cp .aliases ~
	cp .bashrc ~
	cp .zshrc ~
ZSH="${HOME}/.oh-my-zsh"
# uncomment for hacky stuff to bypass MITM cert probs
INS_GIT='SSL_NO_VERIFY=true'
INS_WGET='--no-check-certificate'
INS_CURL='-k'
brew:
	bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
	# echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> /home/al/.profile
	# eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
	sudo apt-get install build-essential

zsh:
	sudo apt install -y zsh
	# mkdir -p ${ZSH}
	sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
	cp -r ohmy/custom/themes/* ${ZSH}/custom/themes
	cp -r ohmy/custom/plugins/* ${ZSH}/custom/plugins
    # FONTS
	mkdir -p ~/.local/share/fonts/ ~/.config/fontconfig/conf.d/
	cp -r ohmy/fonts/* ~/.local/share/fonts/
	cp -r ohmy/fontconfig/* ~/.config/fontconfig/conf.d/
	fc-cache -vf ~/.local/share/fonts/


gcloud:
	echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
	sudo apt-get install apt-transport-https ca-certificates gnupg
	curl ${INS_CURL} https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
	sudo apt-get update && sudo apt-get install google-cloud-sdk
	sudo apt-get install kubectl

helm:
	wget ${INS_WGET} https://get.helm.sh/helm-v3.5.4-linux-amd64.tar.gz	
	tar -zxvf helm*.tar.gz
	mv linux-amd64/helm /usr/local/bin/helm


everything: aliases zsh gcloud helm
