#!/usr/bin/bash
# a menu driven installer for all the new stuff I need on a reinstall (desktop/laptop)


red='\e[0;31m'
RED='\e[1;31m'
blue='\e[0;34m'
BLUE='\e[1;34m'
cyan='\e[0;36m'
CYAN='\e[1;36m'
green='\e[0;32m'
GREEN='\e[1;32m'
YELLOW='\e[1;33m'
purple='\e[1;35m'
NOCOL='\e[0m'


export OHMY="${HOME}/.oh-my-zsh"

# aliases
function do_configs() {
    cp .aliases ~
    cp .bashrc ~
    cp .zshrc ~

}

function install_deps() {
    sudo apt-get install -y \
    build-essential zsh apt-transport-https ca-certificates gnupg make git ansible curl fontconfig
}

function install_brew() {
    which brew > /dev/null
    if [[ $? -eq 0 ]]; then
        echo -e "${CYAN}BREW already installed!" 
    else
        bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
        echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> /home/al/.profile && \
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
    fi
}

function do_zsh() {
    # normal
    #sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    # unattended... needs shell change afterwards
    test -f ~/.oh-my-zsh/oh-my-zsh.sh || sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended && \
    which zsh && sudo usermod -s $(which zsh) al
    cp -r ohmy/custom/themes/* ${OHMY}/custom/themes
    cp -r ohmy/custom/plugins/* ${OHMY}/custom/plugins
    git clone https://github.com/romkatv/powerlevel10k.git $ZSH_CUSTOM/themes/powerlevel10k
    cp .p10k.zsh ~

    # FONTS
    mkdir -p ~/.local/share/fonts/ ~/.config/fontconfig/conf.d/
    cp -r ohmy/fonts/* ~/.local/share/fonts/
    cp -r ohmy/fontconfig/* ~/.config/fontconfig/conf.d/
    fc-cache -vf ~/.local/share/fonts/
    echo -e "restore my .zshrc"
    cp .zshrc ~
}

function install_gcloud() {
    which gcloud > /dev/null
    if [[ $? -eq 0 ]]; then
        echo -e "${CYAN}gcloud already installed!" 
    else
        echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" \
        | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
        curl  https://packages.cloud.google.com/apt/doc/apt-key.gpg \
        | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add - && \
        sudo apt-get update && sudo apt-get install google-cloud-sdk -y
    fi
    # sudo apt-get install kubectl
}

function install_brew_pkgs() {
    echo -e "Install some things with brew"
    which brew && brew install helm && brew install kubectl && brew install terraform
}

# HOSTS
function hosts_file() {
if [[ $(egrep "red{1,4}" /etc/hosts|wc -l) -eq 3 ]]; then
        echo -e "${CYAN}hosts file already setup!" 
else
echo "10.1.1.111 black1.ateam.local black1
10.1.1.5 red1.ateam.local red1
10.1.1.25 red2.ateam.local red2
10.1.1.45 red4.ateam.local red4
" |sudo tee -a /etc/hosts
fi
}

function install_rancher_desktop() {
    curl -s https://download.opensuse.org/repositories/isv:/Rancher:/stable/deb/Release.key | gpg --dearmor | sudo dd status=none of=/usr/share/keyrings/isv-rancher-stable-archive-keyring.gpg
    echo 'deb [signed-by=/usr/share/keyrings/isv-rancher-stable-archive-keyring.gpg] https://download.opensuse.org/repositories/isv:/Rancher:/stable/deb/ ./' | sudo dd status=none of=/etc/apt/sources.list.d/isv-rancher-stable.list
    sudo apt update
    sudo apt install rancher-desktop
}

function confirm() {
    echo -e "\n${YELLOW}Installing ${1}. READY? [Y/n]${NOCOL} \c"
    read -n1 -p ">" resp
    case $resp in
        y|Y|"")
        ;;
        *)
            echo -e "\n\n${RED}Quitting!${NOCOL}"
            sleep 1
            exit 1
        ;;
        esac
}
##--------------------------------------------------

function main() {
    clear
    echo -e "\n${CYAN}==================== MENU ====================${NOCOL}"
    echo -e " 1)  Install Everything!"
    echo -e " 2)  Config/Aliases only"
    echo -e " 3)  Aliases, install deps, hosts file"
    echo -e " 4)  zsh"
    echo -e " 5)  brew"
    echo -e " 6)  brew & packages (helm, kubectl... )"
    echo -e " 7)  gcloud"
    echo -e " 8)  Rancher desktop"
    echo -e " ${red}q${NOCOL})  QUIT"
    echo
    read -n1 -p "-> " ans

    case $ans in
        x)
            confirm "TESTING"
            echo "pew pew"
            exit 0
        ;;
        1)
            confirm "EVERYTHING"
            do_configs
            install_deps
            install_brew
            do_zsh
            install_gcloud
            install_brew_pkgs
            install_rancher_desktop
            hosts_file
            go_back
        ;;
        2)
            confirm "Aliases, Deps & hosts file"
            do_configs
            go_back
        ;;
        3)
            confirm "Aliases, Deps & hosts file"
            do_configs
            install_deps
            hosts_file
            go_back
        ;;
        4)
            confirm "Zsh"
            do_zsh
            go_back
        ;;
        5)
            confirm "Brew"
            install_brew
            go_back
        ;;
        6)
            confirm "Brew & Packages"
            install_brew
            install_brew_pkgs
            go_back
        ;;
        7)
            confirm "Gcloud"
            install_gcloud
            go_back
        ;;
        8)
            confirm "Rancher Desktop"
            install_rancher_desktop
            go_back
        ;;
        *)
            exit 0
        ;;
    esac
}

function go_back() {
    echo -e "${GREEN}[ DONE ]${NOCOL}"
    echo -e "Press ENTER to continue... \c"
    read junk
    main
}

############# RUN #################

main